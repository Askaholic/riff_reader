use byteorder::{LittleEndian, ReadBytesExt};
use hexplay::HexViewBuilder;

use std;

use std::fmt::Display;
use std::fmt::Formatter;

use super::error::Error;

pub mod wave;
use self::wave::WaveFmtSubChunk;

pub enum RiffChunkType {
    Generic(RiffFileChunk),
    Wave(WaveFmtSubChunk)
}

impl Display for RiffChunkType {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        use self::RiffChunkType::*;

        match *self {
            Generic(ref chunk) => chunk.fmt(f),
            Wave(ref chunk) => chunk.fmt(f)
        }
    }
}

pub trait RiffChunk {
    fn id(&self) -> &[u8; 4];
    fn size(&self) -> u32;
}

pub trait DisplayableRiffChunk: Display + RiffChunk {}
impl<T> DisplayableRiffChunk for T where T: Display + RiffChunk {}

pub trait FromRiffFileChunk
    where Self: std::marker::Sized {
    fn from_chunk(riff_chunk: &RiffFileChunk) -> Result<Self, Error>;
}

pub struct RiffFile {
    id: [u8; 4],
    size: u32,
    format: [u8; 4],
    data: Vec<RiffChunkType>
}

impl RiffFile {
    pub fn empty() -> RiffFile {
        RiffFile {
            id: [0; 4],
            size: 0,
            format: [0; 4],
            data: vec![]
        }
    }

    pub fn format(&self) -> &[u8; 4] { &self.format }
    fn data(&self) -> &Vec<RiffChunkType> { &self.data }
}

impl RiffChunk for RiffFile {
    fn id(&self) -> &[u8; 4] { &self.id }
    fn size(&self) -> u32 { self.size }
}

impl Display for RiffFile {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "Header:\n")?;
        write!(f, "    Id: {}\n", String::from_utf8_lossy(self.id()))?;
        write!(f, "    Size: {} bytes\n", self.size())?;
        write!(f, "    Format: {}\n", String::from_utf8_lossy(self.format()))?;

        for chunk in self.data() {
            write!(f, "{}", chunk)?;
        }
        Ok(())
    }
}

#[derive(Debug)]
pub struct RiffFileChunk {
    id: [u8; 4],
    size: u32,
    data: Vec<u8>
}

impl RiffFileChunk {
    pub fn empty() -> RiffFileChunk {
        RiffFileChunk {
            id: [0; 4],
            size: 0,
            data: vec![]
        }
    }
    fn data(&self) -> &Vec<u8> { &self.data }
}

impl RiffChunk for RiffFileChunk {
    fn id(&self) -> &[u8; 4] { &self.id }
    fn size(&self) -> u32 { self.size }
}

impl Display for RiffFileChunk {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "Chunk:\n")?;
        write!(f, "    Id: {}\n", String::from_utf8_lossy(self.id()))?;
        write!(f, "    Size: {} bytes\n", self.size())?;
        let view = HexViewBuilder::new(self.data())
            .address_offset(0)
            .row_width(16)
            .finish();
        write!(f, "    Data:\n{}\n", view)
    }
}

pub fn read_riff_from_stream<T>(stream: &mut T) -> Result<RiffFile, Error>
where T: std::io::Read {
    let riff = read_riff_header(stream, RiffFile::empty())?;
    let riff = read_riff_data(stream, riff)?;

    Ok(riff)
}

pub fn read_riff_header<T>(stream: &mut T, mut riff: RiffFile) ->  Result<RiffFile, Error>
where T: std::io::Read {

    stream.read_exact(&mut riff.id)?;
    if riff.id != [0x52, 0x49, 0x46, 0x46] {
        return Err(Error::new(String::from("Id does not match")));
    }
    riff.size = stream.read_u32::<LittleEndian>()?;
    stream.read_exact(&mut riff.format)?;

    Ok(riff)
}

pub fn read_riff_data<T>(stream: &mut T, mut riff: RiffFile) ->  Result<RiffFile, Error>
where T: std::io::Read {
    let mut bytes_read = 4u32;

    while bytes_read < riff.size() {
        let chunk = read_riff_chunk(stream)?;
        bytes_read += chunk.size() + 8;
        riff.data.push(RiffChunkType::Generic(chunk));
    }

    Ok(riff)
}


pub fn read_riff_chunk<T>(stream: &mut T) ->  Result<RiffFileChunk, Error>
where T: std::io::Read {
    let mut chunk = RiffFileChunk::empty();

    stream.read_exact(&mut chunk.id)?;
    chunk.size = stream.read_u32::<LittleEndian>()?;

    chunk.data.reserve(chunk.size as usize);
    unsafe {
        chunk.data.set_len(chunk.size as usize);
    }
    if chunk.data.capacity() != chunk.size as usize {
        panic!("Vector memory: {}", chunk.data.capacity());
    }

    stream.read_exact(&mut chunk.data.as_mut_slice())?;

    Ok(chunk)
}
