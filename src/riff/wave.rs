use super::{RiffFile, RiffChunk, RiffFileChunk, FromRiffFileChunk, RiffChunkType};
use super::super::error::Error;

use byteorder::{LittleEndian, ReadBytesExt};

use std;
use std::fmt::Display;
use std::fmt::Formatter;

use std::io::Cursor;

pub struct WaveFmtSubChunk {
    id: [u8; 4],
    size: u32,
    data: WaveFmtSubChunkData
}

impl WaveFmtSubChunk {
    fn data(&self) -> &WaveFmtSubChunkData { &self.data }
}

impl RiffChunk for WaveFmtSubChunk {
    fn id(&self) -> &[u8; 4] { &self.id }
    fn size(&self) -> u32 { self.size }
}

impl FromRiffFileChunk for WaveFmtSubChunk {
    fn from_chunk(riff_chunk: &RiffFileChunk) -> Result<Self, Error> {
        let mut cursor = Cursor::new(&riff_chunk.data);

        Ok(WaveFmtSubChunk {
            id: riff_chunk.id,
            size: riff_chunk.size,
            data: WaveFmtSubChunkData {
                audio_format: cursor.read_u16::<LittleEndian>()?,
                num_channels: cursor.read_u16::<LittleEndian>()?,
                sample_rate: cursor.read_u32::<LittleEndian>()?,
                byte_rate: cursor.read_u32::<LittleEndian>()?,
                block_align: cursor.read_u16::<LittleEndian>()?,
                bits_per_sample: cursor.read_u16::<LittleEndian>()?
            }
        })
    }
}

impl Display for WaveFmtSubChunk {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "Fmt Chunk:\n")?;
        write!(f, "    Id: {}\n", String::from_utf8_lossy(&self.id))?;
        write!(f, "    Size: {} bytes\n", self.size)?;
        write!(f, "{}", self.data)
    }
}

pub struct WaveFmtSubChunkData {
    audio_format: u16,
    num_channels: u16,
    sample_rate: u32,
    byte_rate: u32,
    block_align: u16,
    bits_per_sample: u16
}

impl Display for WaveFmtSubChunkData {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "    AudioFormat: {}\n", self.audio_format)?;
        write!(f, "    NumChannels: {}\n", self.num_channels)?;
        write!(f, "    SampleRate: {} Hz\n", self.sample_rate)?;
        write!(f, "    ByteRate: {} bytes/sec\n", self.byte_rate)?;
        write!(f, "    BlockAlign: {}\n", self.block_align)?;
        write!(f, "    BitsPerSample: {}\n", self.bits_per_sample)
    }
}

pub fn parse_wave_from_riff(mut riff: RiffFile) -> RiffFile {
    let new_chunk = _get_wave_from_riff(&riff.data[0]);

    if let Some(chunk) = new_chunk {
        riff.data.push(chunk);
        riff.data.swap_remove(0);
    }

    riff
}

fn _get_wave_from_riff(first_item: &RiffChunkType) -> Option<RiffChunkType> {
    if let RiffChunkType::Generic(ref chunk) = first_item {
        let new_item =  RiffChunkType::Wave(
            WaveFmtSubChunk::from_chunk(chunk).unwrap()
        );
        return Some(new_item)
    }
    None
}
