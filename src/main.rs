extern crate riff_reader;

fn main() {
    let conf = match riff_reader::parse_args(std::env::args()) {
        Ok(c) => c,
        Err(e) => {
            println!("Could not parse arguments: {}", e);
            std::process::exit(-1);
        }
    };

    if let Err(e) = riff_reader::run(conf) {
        println!("{}", e);
    }
}
