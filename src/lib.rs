extern crate byteorder;
extern crate hexplay;

use std::io::BufReader;
use std::fs::File;

mod riff;
use riff::{read_riff_from_stream};
use riff::wave::parse_wave_from_riff;

mod error;
use error::Error;

pub struct Config {
    filename: String
}

pub fn parse_args(mut args: std::env::Args) -> Result<Config, &'static str> {
    args.next();

    match args.next() {
        Some(arg) => Ok(Config{ filename: arg }),
        None => Err("Missing filename")
    }
}

pub fn run(conf: Config) -> Result<(), Error> {
    let file = File::open(&conf.filename)?;
    let mut file = BufReader::new(file);

    let riff = match read_riff_from_stream(&mut file) {
        Ok(r) => r,
        Err(e) => return Err(Error::new(format!("Invalid RIFF file: {}", e)))
    };


    let riff = match riff.format() {
        // WAVE TODO: use macro here?
        [87, 65, 86, 69] => parse_wave_from_riff(riff),
        _ => riff
    };

    println!("{}", riff);

    Ok(())
}
